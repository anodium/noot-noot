# noot-noot
Chrome extension that replaces swear words with NOOT NOOT

## Installation
You can find the extension pre-packaged and signed [here](https://chrome.google.com/webstore/detail/noot-noot/pefpfonanmllmikkgkcibbmijobflocb).
Open the link on Google Chrome or Chromium to install it.

You can also clone the repository and install the `src/` directory
as an unpacked extension for a bleeding edge release.

## Usage
The extension will run in the background, silently replacing
swear words with "NOOT NOOT". You can disable the extension
by navigating to `chrome://extensions` and disabling it there.
