function noot(node, list) {
    "use strict";

    var n = node.nodeValue;

    for (var word in list) {
        n = n.replace(list[word], "NOOT NOOT");
    }

    node.nodeValue = n;
}

function walk(node, list) {
    "use strict";

    var child, next;

    switch (node.nodeType) {
        case 1:
        case 9:
        case 11:
            child = node.firstChild;
            while (child) {
                next = child.nextSibling;
                walk(child, list);
                child = next;
            }
            break;

        case 3:
            noot(node, list);
            break;
    }
}

function update(records) {
    "use strict";

    for (var record in records) {
        walk(records[record].addedNodes[0], list);
    }
}

function main() {
    "use strict";

    var xhr = new XMLHttpRequest();

    xhr.addEventListener("load", function() {
        list = JSON.parse(this.response).swears;
        for (var word in list) {
            list[word] = new RegExp(list[word], "gi");
        }

        walk(document.body, list);

        mo = new MutationObserver(update);
        mo.observe(document.body, {
            childList: true,
            attributes: false,
            characterData: true,
            subtree: true,
            attributeOldValue: false,
            characterDataOldValue: false
        });
    });

    xhr.open("GET", chrome.extension.getURL("swear.json"), true);
    xhr.send();
}

(function() {
    "use strict";

    var list;
    main();
})();
