# Change Log
All notable changes to this project will be documented in this file.

## [v1.0.0] [Bajoo](../../releases/tag/v1.0.0) 2015-11-22
* Initial release

## [v1.1.0] [Chips](../../releases/tag/v1.1.0) 2015-11-23
### Added
* Support for dynamic websites
